package it.univr.android.flickrclient.view;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.view.ActionMode;
import android.widget.ArrayAdapter;

import it.univr.android.flickrclient.FlickrClientApplication;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.model.Model;

public class PicturesListActivity extends ListActivity implements View {
    private MVC mvc;
    private final static String TAG = PicturesListActivity.class.getName();
    private final static String PARAM_SEARCH_STRING = TAG + ".search_string";

    public static void start(Context parent, String searchString) {
        Intent intent = new Intent(parent, PicturesListActivity.class);
        intent.putExtra(PARAM_SEARCH_STRING, searchString);
        parent.startActivity(intent);
    }

    @Override @UiThread
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FlickrClientApplication app = (FlickrClientApplication) getApplication();
        mvc = app.getMVC();
        mvc.controller.fetchPictureInfos(getIntent().getStringExtra(PARAM_SEARCH_STRING));
    }

    @Override @UiThread
    protected void onStart() {
        super.onStart();
        mvc.register(this);
        onModelChanged();
    }

    @Override @UiThread
    protected void onStop() {
        mvc.unregister(this);
        super.onStop();
    }

    @Override @UiThread
    public void onModelChanged() {
        ArrayAdapter<Model.PictureInfo> adapter = new ArrayAdapter<>
                (this, android.R.layout.simple_list_item_1, mvc.model.getPictureInfos());
        setListAdapter(adapter);
    }
}
