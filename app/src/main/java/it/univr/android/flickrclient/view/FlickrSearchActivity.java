package it.univr.android.flickrclient.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.widget.Button;
import android.widget.EditText;

import it.univr.android.flickrclient.FlickrClientApplication;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;

public class FlickrSearchActivity extends Activity implements View {
    private EditText searchStringEditText;
    private Button send;
    private MVC mvc;

    @Override @UiThread
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FlickrClientApplication app = (FlickrClientApplication) getApplication();
        mvc = app.getMVC();

        setContentView(R.layout.activity_search_flickr);

        searchStringEditText = (EditText) findViewById(R.id.insert_search_string);
        send = (Button) findViewById(R.id.send_search_string);
        send.setOnClickListener(
                view -> PicturesListActivity.start(this, searchStringEditText.getText().toString()));
    }

    @Override @UiThread
    protected void onStart() {
        super.onStart();
        mvc.register(this);
        onModelChanged();
    }

    @Override @UiThread
    protected void onStop() {
        mvc.unregister(this);
        super.onStop();
    }

    @Override @UiThread
    public void onModelChanged() {
        // nothing to do here
    }
}