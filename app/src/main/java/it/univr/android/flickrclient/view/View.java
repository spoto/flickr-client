package it.univr.android.flickrclient.view;

import android.support.annotation.UiThread;

public interface View {

    @UiThread
    void onModelChanged();
}
