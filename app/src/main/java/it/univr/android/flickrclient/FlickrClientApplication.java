package it.univr.android.flickrclient;

import android.app.Application;

import it.univr.android.flickrclient.controller.Controller;
import it.univr.android.flickrclient.model.Model;

public class FlickrClientApplication extends Application {
    private MVC mvc;

    @Override
    public void onCreate() {
        super.onCreate();

        mvc = new MVC(new Model(), new Controller());
    }

    public MVC getMVC() {
        return mvc;
    }
}
