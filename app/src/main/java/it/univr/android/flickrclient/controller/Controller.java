package it.univr.android.flickrclient.controller;

import android.os.AsyncTask;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.model.Model;

public class Controller {
    private final static String TAG = Controller.class.getName();
    private MVC mvc;

    public void setMVC(MVC mvc) {
        this.mvc = mvc;
    }

    @UiThread
    public void fetchPictureInfos(String searchString) {
        new PicturesInfoFetcher().execute(searchString);
    }

    private class PicturesInfoFetcher extends AsyncTask<String, Void, Iterable<Model.PictureInfo>> {

        @Override @WorkerThread
        protected Iterable<Model.PictureInfo> doInBackground(String... args) {
            return fetchPictureInfos(args[0]);
        }

        @Override @UiThread
        protected void onPostExecute(Iterable<Model.PictureInfo> pictureInfos) {
            mvc.model.storePictureInfos(pictureInfos);
        }

        private final static String API_KEY = "388f5641e6dc1ecac49678a156f375df";

        @WorkerThread
        private Iterable<Model.PictureInfo> fetchPictureInfos(String searchString) {
            String query = String.format("https://api.flickr.com/services/rest?method=flickr.photos.search&api_key=%s&text=%s&extras=url_z,description,tags&per_page=50",
                    API_KEY,
                    searchString);

            Log.d(TAG, query);
            try {
                URL url = new URL(query);
                URLConnection conn = url.openConnection();
                String answer = "";

                BufferedReader in = null;
                try {
                    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    String line;
                    while ((line = in.readLine()) != null) {
                        answer += line + "\n";
                        Log.d(TAG, line);
                    }
                }
                finally {
                    if (in != null)
                        in.close();
                }

                return parse(answer);
            }
            catch (IOException e) {
                return Collections.emptyList();
            }
        }

        private Iterable<Model.PictureInfo> parse(String xml) {
            List<Model.PictureInfo> infos = new LinkedList<>();

            int nextPhoto = -1;
            do {
                nextPhoto = xml.indexOf("<photo id", nextPhoto + 1);
                if (nextPhoto >= 0) {
                    Log.d(TAG, "nextPhoto = " + nextPhoto);
                    int titlePos = xml.indexOf("title=", nextPhoto) + 7;
                    int url_zPos = xml.indexOf("url_z=", nextPhoto) + 7;
                    String title = xml.substring(titlePos, xml.indexOf("\"", titlePos + 1));
                    String url_z = xml.substring(url_zPos, xml.indexOf("\"", url_zPos + 1));
                    infos.add(new Model.PictureInfo(title, url_z));
                }
            }
            while (nextPhoto != -1);

            return infos;
        }
    }
}